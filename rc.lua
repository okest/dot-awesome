-- INCLUDES
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
awful.rules = require("awful.rules")
awful.util.shell = "/usr/bin/sh"

local wibox = require("wibox")
local beautiful = require("beautiful")
local naughty = require("naughty")
naughty.config.defaults.position = "bottom_right"
naughty.config.padding = 10

local menubar = require("menubar")
local lain = require("lain")
local markup = lain.util.markup

-- NOTE: PLATFORM
local hostname = string.sub(awful.util.pread("uname -n"), 1, 4)
local w = require(hostname)
-- // INCLUDES

-- ERRORS
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

do
    local in_error = false
    awesome.connect_signal(
        "debug::error",
        function (err)
            -- Make sure we don't go into an endless error loop
            if in_error then return end
            in_error = true

            naughty.notify({ preset = naughty.config.presets.critical,
                             title = "Oops, an error happened!",
                             text = err })
            in_error = false
        end
    )
end
-- // ERRORS

-- EARLY SETUP
beautiful.init("~/.config/awesome/theme.lua")

editor = os.getenv("EDITOR") or "emacsclient"
editor_cmd = editor

terminal = "termite"
menubar.utils.terminal = terminal -- Set the terminal for applications that require it

modkey = "Mod4"

local program_starter = "rofi -show run"
    .. " -bg \\" .. beautiful.bg_normal
    .. " -fg \\" .. beautiful.fg_normal

local layouts = {
    lain.layout.uselesstile
}

for s = 1, screen.count() do
    w.tags[s] = awful.tag(w.tags[s], s, layouts[1])
end
-- // EARLY SETUP

-- STARTUP
awful.util.spawn_with_shell("xrandr --output DVI-D-0 --left-of DVI-I-0 --primary")

if beautiful.wallpaper then
    for s = 1, screen.count() do
        gears.wallpaper.maximized(beautiful.wallpaper, s, true)
    end
end
-- // STARTUP

-- WIDGETS
mpd_widget = lain.widgets.mpd(
    {
        timeout = 5,
        settings = function ()

            -- notification changes
            mpd_notification_preset.text =
                string.format("%s - %s\n%s (%s)",
                              mpd_now.artist, mpd_now.title,
                              mpd_now.album, mpd_now.date
                )

            local colour = beautiful.green or "#FFFFFF"
            local icon   = "  " -- two spaces looks best
            if mpd_now.state == "play" then
                colour = beautiful.green
                icon   = "  "
            elseif mpd_now.state == "pause" then
                colour = beautiful.yellow
                icon   = "  "
            else
                colour = beautiful.red
            end

            if mpd_now.artist == "N/A" then
                widget:set_markup(markup(colour, icon .. "no music playing"))
            else
                widget:set_markup(markup(colour, icon .. mpd_now.artist .. " - " .. mpd_now.title))
            end
        end
    }
)

mytaglist = {}
mytaglist.buttons = awful.util.table.join(
    awful.button({ }, 1, awful.tag.viewonly),
    awful.button({ modkey }, 1, awful.client.movetotag),
    awful.button({ }, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, awful.client.toggletag),
    awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
    awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
)

mytasklist = {}
mytasklist.buttons = awful.util.table.join(
    awful.button({ }, 1,
        function (c)
            if c == client.focus then
                c.minimized = true
            else
                -- Without this, the following :isvisible() makes no sense
                c.minimized = false
                if not c:isvisible() then
                    awful.tag.viewonly(c:tags()[1])
                end
                -- This will also un-minimize the client, if needed
                client.focus = c
                c:raise()
            end
        end
    ),
    awful.button({ }, 3,
        function ()
            if instance then
                instance:hide()
                instance = nil
            else
                instance = awful.menu.clients({theme = { width = 250 }})
            end
        end
    ),
    awful.button({ }, 4,
        function ()
            awful.client.focus.byidx(1)
            if client.focus then client.focus:raise() end
        end
    ),
    awful.button({ }, 5,
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end
    )
)

local clock_widget  = awful.widget.textclock(" %F %A %I:%M %p ", 60)
local prompt_widget = {}

local separator = wibox.widget.textbox(" ")
-- // WIDGETS

-- TOPBAR
local topbar = {}
for s = 1, screen.count() do
    prompt_widget[s]  = awful.widget.prompt()
    mytaglist[s]  = awful.widget.taglist(s, awful.widget.taglist.filter.noempty, mytaglist.buttons)
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)
    topbar[s]     = awful.wibox({ position = "top", screen = s })

    local left_layout = wibox.layout.fixed.horizontal()
    left_layout:add(mytaglist[s])
    left_layout:add(prompt_widget[s])

    local middle_layout = wibox.layout.fixed.horizontal()
    middle_layout:add(mytasklist[s])

    -- Widgets that are aligned to the right
    -- last one listed here will be rightmost
    local right_layout = wibox.layout.fixed.horizontal()
    right_layout:add(mpd_widget)
    right_layout:add(separator)
    right_layout:add(clock_widget)
    if s == 2 then
        right_layout:add(separator)
        right_layout:add(wibox.widget.systray())
    end

    -- Now bring it all together (with the tasklist in the middle)
    local layout = wibox.layout.align.horizontal()

    layout:set_left(left_layout)
    layout:set_middle(middle_layout)
    layout:set_right(right_layout)

    topbar[s]:set_widget(layout)

end
-- // TOPBAR

-- BINDINGS
globalkeys = awful.util.table.join(
    awful.key({ modkey, }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey, }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey, }, "Escape", awful.tag.history.restore),

    awful.key({ modkey, }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey, }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx( 1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx(-1)    end),
    awful.key({ modkey, "Alt"     }, "j", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Alt"     }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end
    ),

    -- Standard program
    awful.key({ modkey            }, "Return", function () awful.util.spawn(terminal)   end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Control" }, "e", awesome.quit),
    awful.key({ modkey            }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey            }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey            }, "space", function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),

    awful.key({ modkey, "Control" }, "n", awful.client.restore),

    awful.key({ modkey }, "x",
        function ()
            awful.prompt.run({ prompt = "Run Lua code: " },
                prompt_widget[mouse.screen].widget,
                awful.util.eval,
                nil,
                awful.util.getdir("cache") .. "/history_eval"
            )
        end
    ),

    awful.key({ modkey }, "d", function () awful.util.spawn(program_starter) end)
)

for i = 1,#w.globalkeys do
    globalkeys = awful.util.table.join(globalkeys, w.globalkeys[i])
end

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(
        globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      local tag = awful.tag.gettags(screen)[i]
                      awful.tag.viewonly(tag)
                  end
        ),
        -- Toggle tag.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
            function ()
                local screen = mouse.screen
                local tag = awful.tag.gettags(screen)[i]
                if tag then
                    awful.tag.viewtoggle(tag)
                end
            end
        ),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
            function ()
                if client.focus then
                    local tag = awful.tag.gettags(client.focus.screen)[i]
                    if tag then
                        awful.client.movetotag(tag)
                    end
                end
            end
        ),
        -- Toggle tag.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
            function ()
                if client.focus then
                    local tag = awful.tag.gettags(client.focus.screen)[i]
                    if tag then
                        awful.client.toggletag(tag)
                    end
                end
            end
        )
    )
end

-- keys for interacting with clients
clientkeys = awful.util.table.join(
    awful.key({ modkey,         }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, "Shift" }, "q",      function (c) c:kill()                         end),
    awful.key({ modkey, "Shift" }, "f",      awful.client.floating.toggle                     ),
    awful.key({ modkey, "Shift" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,         }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey,         }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,         }, "n",      function (c) c.minimized = true               end),
    awful.key({ modkey,         }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

-- mouse buttons
clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize)
)

-- Set keys
root.keys(globalkeys)
-- // BINDINGS

-- RULES
-- (apply to new clients through the "manage" signal)
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = {},
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus        = awful.client.focus.filter,
                     raise        = true,
                     keys         = clientkeys,
                     buttons      = clientbuttons }},
    { rule = {class = "Firefox"},
      properties = {tag = w.tags[1][1]},
      callback   = function (c)
          s = 1
          w.tags[s][1].name = w.special_tags[s][1]
      end
    },
    { rule       = {class = "Emacs"},
      properties = {tag   = w.tags[1][3]},
      callback   = function (c)
          s = 1
          w.tags[s][3].name = w.special_tags[s][3]
      end
    },
    -- floating rules
    { rule_any   = {class = {"pinentry", "File-roller", "Variety", "Pavucontrol", "Termite"},
                    title = {"Screenshot Uploader", "About Mozilla Firefox"}},
      properties = {floating = true}},
}

-- NOTE: PLATFORM
awful.rules.rules = awful.util.table.join(awful.rules.rules, table.unpack(w.rules))
-- // RULES

-- SIGNALS
-- Signal function to execute when a new client appears.
client.connect_signal("manage",
    function (c, startup)
        -- Enable sloppy focus
        c:connect_signal(
            "mouse::enter",
            function(c)
                if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
                and awful.client.focus.filter(c) then
                    client.focus = c
                end
            end
        )

        if not startup then
            -- Set the windows at the slave,
            -- i.e. put it at the end of others instead of setting it master.
            --awful.client.setslave(c)

            -- Put windows in a smart way, only if they does not set an initial position.
            if not c.size_hints.user_position and not c.size_hints.program_position then
                awful.placement.no_overlap(c)
                awful.placement.no_offscreen(c)
            end
        end
    end
)

client.connect_signal("focus",   function(c) c.border_color = beautiful.border_focus  end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- // SIGNALS

