local awful = require("awful")
local w = {}

w.mpc = "env MPD_HOST=wmus@localhost mpc" -- no trailing space

function w.mpc_adj_volume(amount)
    local cmd = ""
    if amount < 0 then
        cmd = w.mpc .. " volume "  .. amount
    else
        cmd = w.mpc .. " volume +" .. amount
    end

    awful.util.spawn_with_shell(cmd)
end

function w.mpc_toggle(widget)
    awful.util.spawn(w.mpc .. "toggle")
    if widget then
        widget:update()
    end
end

function w.mpc_next()
    awful.util.spawn(w.mpc .. "next")
    if widget then
        widget:update()
    end
end

function w.mpc_prev()
    awful.util.spawn(w.mpc .. "prev")
    if widget then
        widget:update()
    end
end

w.dev_null = " > /dev/null"
w.wallpaper_next  = "variety --next"     .. w.dev_null
w.wallpaper_prev  = "variety --previous" .. w.dev_null
w.wallpaper_trash = "variety --trash"    .. w.dev_null
w.wallpaper_fav   = "variety --favorite" .. w.dev_null

function w.get_other_screen(s)
    if s >= screen.count() then
        return 1
    else
        return s + 1
    end
end

function w.move_mouse_to_screen(s)
    return -- FIXME: I uh, lost this
end

-- TAGS
w.special_tags = {
    { "", "", "", 4, 5, 6, 7, 8, 9 }, -- first monitor
    { "", "", "", 4, 5, 6, 7, 8, 9 }  -- second monitor
}

w.tags = {
    { 1, 2, 3, 4, 5, 6, 7, 8, 9 },
    { 1, 2, 3, 4, 5, 6, 7, 8, 9 }
}
-- // TAGS

-- KEYBINDS
w.globalkeys = {
    -- music (mpd/mpc)
    awful.key({}, "XF86AudioPlay",        w.mpc_toggle),
    awful.key({}, "XF86AudioRaiseVolume", w.mpc_adj_volume( 5)),
    awful.key({}, "XF86AudioLowerVolume", w.mpc_adj_volume(-5)),
    awful.key({}, "XF86AudioNext",        w.mpc_next),
    awful.key({}, "XF86AudioPrev",        w.mpc_prev),

    -- variety
    -- awful.key({modkey, "Mod1"}, "n", awful.util.spawn_with_shell(w.wallpaper_next)),
    -- awful.key({modkey, "Mod1"}, "p", awful.util.spawn_with_shell(w.wallpaper_prev)),
    -- awful.key({modkey, "Mod1"}, "t", awful.util.spawn_with_shell(w.wallpaper_trash)),
    -- awful.key({modkey, "Mod1"}, "n", awful.util.spawn_with_shell(w.wallpaper_fav)),

    -- mouse switcheroo
    awful.key({ modkey }, "`",
        function () w.move_mouse_to_screen(w.get_other_screen(mouse.screen)) end
    ),
    awful.key({ modkey, "Shift" }, "`", awful.client.movetoscreen(awful.mouse.client_under_pointer(),
                                                                  w.get_other_screen(mouse.screen)))
}
-- // KEYBINDS

-- RULES
w.rules = {
    { rule       = {class = "Steam"},
      properties = {tag   = w.tags[1][2]},
      callback   = function (c)
          s = 1
          tags[s][2].name = w.special_tags[s][2]
      end
    },
    -- second monitor
    { rule_any   = {class = {"TeamSpeak 3", "Mumble"}},
      properties = {tag   = w.tags[2][2]},
      callback   = function (c)
          s = 2
          tags[s][2].name = w.special_tags[s][2]
      end
    },
    { rule_any   = {class = {"google-chrome"},
                    title = {" - Twitch - ", " - YouTube - "}},
      properties = {tag   = w.tags[2][3]},
      callback   = function (c)
          s = 2
          tags[s][3].name = w.special_tags[s][3]
      end
    }
}
-- // RULES

return w

